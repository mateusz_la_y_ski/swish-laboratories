redukuj(X1, X2, Y) :- Y is X1 * X2.

% zredukowanie pustej listy nie zmienia początkowej wartości
reduce([], PoczątkowaWartość, PoczątkowaWartość). 

% tak długo, aż lista nie jest pusta, redukuj jej pierwszy 
% element i aktualną wartość.
recude([H|T], AktualnaWartość, WynikowaWartość) :- 
	redukuj(H, AktualnaWartość, NowaWartość),
	reduce(T, NowaWartość, WynikowaWartość).