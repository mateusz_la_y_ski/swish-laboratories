rodzic(kasia,robert).
rodzic(tomek,robert).
rodzic(tomek,eliza).
rodzic(robert,anna).
rodzic(robert,magda).
rodzic(magda,jan).

kobieta(kasia).
kobieta(eliza).
kobieta(magda).
kobieta(anna).

mezczyzna(tomek).
mezczyzna(robert).
mezczyzna(jan).

potomek(X,Y) :-
    rodzic(Y,X).

matka(X,Y) :-
    rodzic(X,Y),
    kobieta(X).

ojciec(X,Y) :-
    rodzic(X,Y),
    mezczyzna(X).

dziadkowie(X,Y) :-
    rodzic(X,Z),
    rodzic(Z,Y).

dziadek(X,Y) :-
    dziadkowie(X,Y),
    mezczyzna(X).

babcia(X,Y) :-
    dziadkowie(X,Y),
    kobieta(X).

siostra(X,Y) :-
    rodzic(Z,X),
    rodzic(Z,Y),
    kobieta(X),
    X \= Y.

brat(X,Y) :-
    rodzic(Z,X),
    rodzic(Z,Y),
    mezczyzna(X),
    X \= Y.

przodek(X,Y) :-
    rodzic(X,Y).

przodek(X,Z) :-
    rodzic(X,Y),
    przodek(Y,Z).

wnuk(X,Z) :-
    rodzic(Y,X),
    rodzic(Z,Y).

ciocia(X,Y) :-
    rodzic(Z,Y),
    siostra(X,Z).

/** <examples>

?- siostra(X, Y).
?- siostra(S, anna), dziadek(D, S).

*/

